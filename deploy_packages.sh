#!/usr/bin/bash

# Mirrors 
mapfile -t packages < mirror.txt

for i in "${packages[@]}"
do
    PACKAGE_NAME=${i%/*}
    PACKAGE_VERSION=`echo ${i##*/} | sed 's/ *$//g'`
    PACKAGE_REFERENCE=${PACKAGE_NAME}/${PACKAGE_VERSION}

    conan install $PACKAGE_REFERENCE
    conan copy $PACKAGE_REFERENCE verbotics+weld-packages/stable --force --all
    
done

conan remote add gitlab https://gitlab.com/api/v4/packages/conan
conan user gitlab-user -r gitlab -p $CI_JOB_TOKEN

conan upload "*@verbotics+weld-packages/stable" --all -r gitlab -c

