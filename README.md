# weld-packages

Package registry for Verbotics Weld and related projects.

## Mirroring packages from the central repository

Some packages are mirrored here, without transitive dependencies. This is a preventative measure in case these packages are removed or deprecated from the Conan central repository

To add or remove a mirrored package, edit the `mirror.txt` file in this repository.
