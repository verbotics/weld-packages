#!/usr/bin/bash

mapfile -t packages < mirror.txt

for i in "${packages[@]}"
do
    PACKAGE_NAME=${i%/*}
    PACKAGE_VERSION=`echo ${i##*/} | sed 's/ *$//g'`
    
    echo "Building package version $PACKAGE_VERSION"
    
    
    PACKAGE_NAMES_STR=$(conan get $i .)
    
    while IFS= read -r line; do
    
        if [[ ${line:0:7} == "Listing" ]]; then
            continue
        fi
        
        if [[ ${line:0:16} == "conan_export.tgz" ]]; then
            continue
        fi
        TRIMMED_LINE=`echo $line | sed 's/ *$//g'`
        
        echo $TRIMMED_LINE
        
        mkdir -p $PACKAGE_NAME
        conan get $i $TRIMMED_LINE --raw > "$PACKAGE_NAME/$TRIMMED_LINE"
        
    done <<< "$PACKAGE_NAMES_STR"

    conan create $PACKAGE_NAME $PACKAGE_NAME/$PACKAGE_VERSION@verbotics/stable
        
    rm -rf $PACKAGE_NAME
done

